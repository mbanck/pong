from st3m.application import Application, ApplicationContext
import captouch
import st3m.run
import math

class Pong(Application):
    R_TOTAL = 120
    R_FIELD = 100
    R_BALL = 5

    BOX_H = 40
    BOX_D = 5
    BOX_W_RAD = math.atan(BOX_H / 2 / R_FIELD)

    PLAYER_RAD_DIST = math.pi / 2 - BOX_W_RAD
    PLAYER_RAD_PER_SEC = math.pi
    PLAYER_SPEED_BUMP = 0.35 * R_FIELD

    LEFT_RAD_INIT = math.pi
    LEFT_RAD_MIN = LEFT_RAD_INIT - PLAYER_RAD_DIST
    LEFT_RAD_MAX = LEFT_RAD_INIT + PLAYER_RAD_DIST

    RIGHT_RAD_INIT = 0.0
    RIGHT_RAD_MIN = RIGHT_RAD_INIT - PLAYER_RAD_DIST
    RIGHT_RAD_MAX = RIGHT_RAD_INIT + PLAYER_RAD_DIST

    def __init__(self, app_ctx: ApplicationContext):
        super().__init__(app_ctx)
        self.score_left = 0
        self.score_right = 0

        self.rad_left = self.LEFT_RAD_INIT
        self.rad_right = self.RIGHT_RAD_INIT

        self.reset_ball()

    def on_enter(self, vm):
        super().on_enter(vm)

        self.score_left = 0
        self.score_right = 0

        self.rad_left = self.LEFT_RAD_INIT
        self.rad_right = self.RIGHT_RAD_INIT

        self.reset_ball()

    def reset_ball(self):

        self.ball_x = 0.0
        self.ball_y = 0.0
        self.ball_vx = -80.0
        self.ball_vy = 0.0

        self.ball_was_in_field = True

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-self.R_TOTAL, -self.R_TOTAL, 2*self.R_TOTAL, 2*self.R_TOTAL).fill()

        # Paint score
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 70
        ctx.rgb(255, 0, 0).move_to(-60, 0).text(f"{self.score_left}").stroke()
        ctx.rgb(0, 255, 0).move_to(+60, 0).text(f"{self.score_right}").stroke()

        # Paint bars
        #   left
        ctx.save()
        ctx.rgb(255, 0, 0).rotate(self.rad_left).rectangle(self.R_FIELD, -self.BOX_H/2, 5, self.BOX_H).fill()
        ctx.restore()
        #   right
        ctx.save()
        ctx.rgb(0, 255, 0).rotate(self.rad_right).rectangle(self.R_FIELD, -self.BOX_H/2, 5, self.BOX_H).fill()
        ctx.restore()

        # Paint ball
        ctx.rgb(255,255,255).arc(self.ball_x, self.ball_y, self.R_BALL, 0, 2*math.pi, 1).fill()

    def think(self, ins: InputState, delta_ms: int) -> None:

        delta_s = delta_ms / 1000

        # get player input
        cs = captouch.read()
        left_move_left = cs.petals[6].pressed
        left_move_right = cs.petals[8].pressed
        right_move_left = cs.petals[2].pressed
        right_move_right = cs.petals[4].pressed

        # process player input
        if left_move_left:
            self.rad_left -= self.PLAYER_RAD_PER_SEC * delta_s
        if left_move_right:
            self.rad_left += self.PLAYER_RAD_PER_SEC * delta_s
        if right_move_left:
            self.rad_right -= self.PLAYER_RAD_PER_SEC * delta_s
        if right_move_right:
            self.rad_right += self.PLAYER_RAD_PER_SEC * delta_s

        # enforce limits
        if self.rad_left < self.LEFT_RAD_MIN:
            self.rad_left = self.LEFT_RAD_MIN
        if self.rad_left > self.LEFT_RAD_MAX:
            self.rad_left = self.LEFT_RAD_MAX
        if self.rad_right < self.RIGHT_RAD_MIN:
            self.rad_right = self.RIGHT_RAD_MIN
        if self.rad_right > self.RIGHT_RAD_MAX:
            self.rad_right = self.RIGHT_RAD_MAX

        # just restart if ball is lost
        if math.sqrt((self.ball_x)**2 + (self.ball_y)**2) > self.R_TOTAL:
          if self.ball_x > 0:
            self.score_left += 1
          if self.ball_x < 0:
            self.score_right += 1
          self.reset_ball()
          return

        # move ball
        self.ball_x += self.ball_vx * delta_s
        self.ball_y += self.ball_vy * delta_s

        # handle ball-box collisions
        ball_in_field = math.sqrt((self.ball_x)**2 + (self.ball_y)**2) < self.R_FIELD - self.R_BALL
        if self.ball_was_in_field and not ball_in_field:

          # radial position
          rad_ball = math.atan2(self.ball_y, self.ball_x)
          if rad_ball < -math.pi/2:
            rad_ball += 2 * math.pi

          # radial velocity
          ball_va = math.atan2(self.ball_vy, self.ball_vx)
          if ball_va < -math.pi/2:
            ball_va += 2 * math.pi
          ball_vr = 1.1 * math.sqrt(self.ball_vx**2 + self.ball_vy**2)

          # collision with left player
          if abs(rad_ball - self.rad_left) <= self.BOX_W_RAD:
            # reflect along player angle
            ball_va = ball_va - 2*(ball_va - self.rad_left)
            # back to cartesian coordinates
            self.ball_vy = -ball_vr * math.sin(ball_va)
            self.ball_vx = -ball_vr * math.cos(ball_va)
            if left_move_left:
              self.ball_vy += math.sin(self.rad_left - math.pi/2) * self.PLAYER_SPEED_BUMP
              self.ball_vx += math.cos(self.rad_left - math.pi/2) * self.PLAYER_SPEED_BUMP
            if left_move_right:
              self.ball_vy += math.sin(self.rad_left + math.pi/2) * self.PLAYER_SPEED_BUMP
              self.ball_vx += math.cos(self.rad_left + math.pi/2) * self.PLAYER_SPEED_BUMP

          # collision with left player
          if abs(rad_ball - self.rad_right) <= self.BOX_W_RAD:
            # reflect along player angle
            ball_va = ball_va - 2*(ball_va - self.rad_right)
            # back to cartesian coordinates
            self.ball_vy = -ball_vr * math.sin(ball_va)
            self.ball_vx = -ball_vr * math.cos(ball_va)

        self.ball_was_in_field = ball_in_field

#if __name__ == '__main__':
  #st3m.run.run_view(Pong(ApplicationContext()))
